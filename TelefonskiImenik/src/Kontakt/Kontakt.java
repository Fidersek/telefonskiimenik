package Kontakt;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.*;

public class Kontakt implements Serializable {

    private static final long serialVersionUID = -5462212594138376176L;

    public static final Pattern ValidNamePattern = Pattern.compile("[A-Za-z ]+", Pattern.CASE_INSENSITIVE);
    public static final Pattern ValidSurnamePattern = Pattern.compile("[A-Za-z ]+", Pattern.CASE_INSENSITIVE);
    public static final Pattern ValidAdressPattern = Pattern.compile("[A-Za-z ]+\\s[0-9]+", Pattern.CASE_INSENSITIVE);
    public static final Pattern ValidMailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);
    public static final Pattern ValidPhoneNumberPattern = Pattern.compile("^\\d{11}$");
    public static final Pattern ValidMobileNumberPattern = Pattern.compile("^\\d{9}$");
    public static final Pattern ValidNote = Pattern.compile("[^,]+");

    private int id;
    private String ime;
    private String priimek;
    private String naslov;
    private String elektronskaPosta;
    private String telefon;
    private String mobilniTelefon;
    private String opomba;

    public Kontakt(int id, String ime, String priimek, String naslov, String elektronskaPosta, String telefon,
            String mobilniTelefon, String opomba) {
        this.id = id;
        this.ime = ime;
        this.priimek = priimek;
        this.naslov = naslov;
        this.elektronskaPosta = elektronskaPosta;
        this.telefon = telefon;
        this.mobilniTelefon = mobilniTelefon;
        this.opomba = opomba;
    }

    public Kontakt() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public String getElektronskaPosta() {
        return elektronskaPosta;
    }

    public void setElektronskaPosta(String elektronskaPosta) {
        this.elektronskaPosta = elektronskaPosta;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getMobilniTelefon() {
        return mobilniTelefon;
    }

    public void setMobilniTelefon(String mobilniTelefon) {
        this.mobilniTelefon = mobilniTelefon;
    }

    public String getOpomba() {
        return opomba;
    }

    public void setOpomba(String opomba) {
        this.opomba = opomba;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Kontakt kontakt = (Kontakt) o;
        return id == kontakt.id && Objects.equals(ime, kontakt.ime) && Objects.equals(priimek, kontakt.priimek)
                && Objects.equals(naslov, kontakt.naslov) && Objects.equals(elektronskaPosta, kontakt.elektronskaPosta)
                && Objects.equals(telefon, kontakt.telefon) && Objects.equals(mobilniTelefon, kontakt.mobilniTelefon)
                && Objects.equals(opomba, kontakt.opomba);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ime, priimek, naslov, elektronskaPosta, telefon, mobilniTelefon, opomba);
    }

    @Override
    public String toString() {
        return "Kontakt{" + "id=" + id + ", ime='" + ime + '\'' + ", priimek='" + priimek + '\'' + ", naslov='" + naslov
                + '\'' + ", elektronskaPosta='" + elektronskaPosta + '\'' + ", telefon='" + telefon + '\''
                + ", mobilniTelefon='" + mobilniTelefon + '\'' + ", opomba='" + opomba + '\'' + '}';
    }

    public static boolean veljavnoIme(String ime) {
        Matcher m = ValidNamePattern.matcher(ime);
        if (!m.matches()) {
            System.out.println("Ime lahko ima samo črke in presledke.");
            return false;
        }
        return true;
    }

    public static boolean veljavniPriimek(String priimek) {
        Matcher m = ValidSurnamePattern.matcher(priimek);
        if (!m.matches()) {
            System.out.println("Priimek lahko ima samo črke in presledke.");
            return false;
        }
        return true;
    }

    public static boolean veljavniNaslov(String naslov) {
        Matcher m = ValidAdressPattern.matcher(naslov);
        if (!m.matches()) {
            System.out.println("Naslov mora biti formata Kraj 1.");
            return false;
        }
        return true;
    }

    public static boolean veljavnaElektronskaPosta(String naslov) {
        Matcher m = ValidMailPattern.matcher(naslov);
        if (!m.matches()) {
            System.out.println("Elektronska posta ni veljavna");
            return false;
        }
        return true;
    }

    public static boolean veljavniTelefon(String telefon) {
        Matcher m = ValidPhoneNumberPattern.matcher(telefon);
        if (!m.matches()) {
            System.out.println("Mobilna številka mora biti formata 02040111111");
            return false;
        }
        return true;
    }

    public static boolean veljavniMobilniTelefon(String mobilni) {
        Matcher m = ValidMobileNumberPattern.matcher(mobilni);
        if (!m.matches()) {
            System.out.println("Mobilna številka mora biti formata 040111111");
            return false;
        }
        return true;
    }

    public static boolean veljavnaOpomba(String opomba) {
        Matcher m = ValidNote.matcher(opomba);
        if (!m.matches()) {
            System.out.println("Opomba nesme vsebovati \",\"");
            return false;
        }
        return true;
    }

    public boolean vsebujeNiz(String search) {
        Pattern pattern = Pattern.compile("[A-Za-z ]*" + search + "[A-Za-z ]*", Pattern.CASE_INSENSITIVE);
        Matcher nameMatcher = pattern.matcher(ime);
        Matcher surnameMatcher = pattern.matcher(priimek);
        if (nameMatcher.matches() || surnameMatcher.matches()) {
            return true;
        }
        return false;
    }

    public String getSqlValues() {
        return "(" + id + ", '" + ime + "', '" + priimek + "', '" + naslov + "', '" + elektronskaPosta + "', '"
                + telefon + "', '" + mobilniTelefon + "', '" + opomba + "')";
    }

}

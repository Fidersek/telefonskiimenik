import TelefonskiImenik.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        // Data
        TelefonskiImenik telefonskiImenik = new TelefonskiImenik();

        // ConnectionData
        String serverAdress = "localhost:1433";
        String databaseName = "MyDatabase";
        String databaseUser = "Matej";
        String databasePassword = "lol123lol";
        String databaseURL = "";

        System.out.println("-----------------------------------");
        // Connect to database
        databaseURL = CreateConnectionURL(serverAdress, databaseName, databaseUser, databasePassword);
        // Load data from database or file
        if (!telefonskiImenik.naloziPodatkeIzPodatkovneBaze(databaseURL)) {
            if (!telefonskiImenik.naloziSerializiranSeznamKontakotv()) {
                System.out.println("Ni bilo najdenih podatov iz baze in datoteke");
            }
        }
        System.out.println("-----------------------------------");

        // Menu
        PrintMenu();
        String action = "";

        while (!"0".equals(action)) {
            action = in.nextLine();
            System.out.println("-----------------------------------");
            switch (action) {
                case "1":
                    telefonskiImenik.izpisiVseKontakte();
                    break;
                case "2":
                    telefonskiImenik.dodajKontakt(in);
                    break;
                case "3":
                    telefonskiImenik.urediKontakt(in);
                    break;
                case "4":
                    telefonskiImenik.izbrisiKontaktPoId(in);
                    break;
                case "5":
                    telefonskiImenik.izpisiKontaktZaId(in);
                    break;
                case "6":
                    telefonskiImenik.izpisiSteviloKontaktov();
                    break;
                case "7":
                    telefonskiImenik.serializirajSeznamKontaktov();
                    break;
                case "8":
                    telefonskiImenik.naloziSerializiranSeznamKontakotv();
                    break;
                case "9":
                    telefonskiImenik.izvoziPodatkeVCsvDatoteko();
                    break;
                case "10":
                    telefonskiImenik.naloziPodatkeIzPodatkovneBaze(databaseURL);
                    break;
                case "11":
                    telefonskiImenik.izvoziPodatkeVPodatkovnoBazo(databaseURL);
                    break;
                case "12":
                    telefonskiImenik.izpisiKontakteZNizom(in);
                    break;
                case "13":
                    // Zamenjaj povezavo
                    System.out.println("Give connection url:");
                    databaseURL = in.nextLine();
                    break;
                case "0":
                    System.exit(0);
                    break;
                default:
                    System.out.println("Napačna izbira!!!");
                    break;
            }

            PrintMenu();
        }
        in.close();
    }

    private static String CreateConnectionURL(String serverAdress, String databaseName, String databaseUser,
            String databasePassword) {
        String connectionUrl;
        connectionUrl = "jdbc:sqlserver://" + serverAdress + ";Database=" + databaseName + ";user=" + databaseUser
                + ";password=" + databasePassword;
        return connectionUrl;
    }

    /**
     * Uporabniku izpišemo menu
     */
    public static void PrintMenu() {

        System.out.println("");
        System.out.println("");
        System.out.println("Aplikacija telefonski imenik:");
        System.out.println("-----------------------------------");
        System.out.println("Akcije:");
        System.out.println("1 - izpiši vse kontakte v imeniku");
        System.out.println("2 - dodaj kontakt v imenik");
        System.out.println("3 - uredi obstoječi kontakt");
        System.out.println("4 - briši kontakt po ID-ju");
        System.out.println("5 - izpiši kontakt po ID-ju");
        System.out.println("6 - izpiši število vseh kontaktov");
        System.out.println("7 - Shrani kontakte na disk (serializacija)");
        System.out.println("8 - Preberi kontake iz serializirano datoteke");
        System.out.println("9 - Izvozi kontakte v csv");
        System.out.println("10 - Beri kontakte iz baze");
        System.out.println("11 - Zapiši kontakte na bazo");
        System.out.println("12 - Iskanje z nizom");
        System.out.println("13 - Zamenjaj url podatkovne baze");
        System.out.println("");
        System.out.println("0 - Izhod iz aplikacije");
        System.out.println("----------------------------------");
        System.out.println("Akcija: ");

    }

}

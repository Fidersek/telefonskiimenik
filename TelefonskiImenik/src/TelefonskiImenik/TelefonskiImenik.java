package TelefonskiImenik;

import Kontakt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class TelefonskiImenik {

    public static final Pattern ValidNumberPattern = Pattern.compile("[0-9]+", Pattern.CASE_INSENSITIVE);

    static final String selectSql = "Select * from TelefonskiImenik";
    static final String insertQuery = "INSERT INTO TelefonskiImenik VALUES (?,?,?,?,?,?,?,?)";
    static final String deleteQuery = "DELETE FROM TelefonskiImenik";

    private List<Kontakt> seznamKontaktov;

    public TelefonskiImenik() {
        seznamKontaktov = new ArrayList<>();
    }

    /**
     * Metoda izpiše vse kontakte
     */
    public void izpisiVseKontakte() {
        for (int i = 0; i < seznamKontaktov.size(); i++) {
            System.out.println(seznamKontaktov.get(i).toString());
        }
    }

    /**
     * Metoda izpiše vse kontakte
     */
    public void izpisiKontakteZNizom(Scanner in) {
        String niz = in.nextLine();
        for (int i = 0; i < seznamKontaktov.size(); i++) {
            if (seznamKontaktov.get(i).vsebujeNiz(niz))
                System.out.println(seznamKontaktov.get(i).toString());
        }
    }

    /**
     * Metaoda doda nov kontakt v imenik
     */
    public void dodajKontakt(Scanner in) {

        boolean validCheck = true;
        Kontakt noviKontakt = new Kontakt();
        String inputString = "";
        // ID
        System.out.println("Vpisi ID:");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (IsPositiveNumber(inputString)) {
                noviKontakt.setId(Integer.parseInt(inputString));
                validCheck = false;
            }
        }
        validCheck = true;
        // Ime
        System.out.println("Vpisi ime:");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavnoIme(inputString)) {
                noviKontakt.setIme(inputString);
                validCheck = false;
            }
        }
        validCheck = true;
        // Priimek
        System.out.println("Vpisite priimek");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavniPriimek(inputString)) {
                noviKontakt.setPriimek(inputString);
                validCheck = false;
            }
        }
        validCheck = true;
        // Naslov
        System.out.println("Vpisite naslov");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavniNaslov(inputString)) {
                noviKontakt.setNaslov(inputString);
                validCheck = false;
            }
        }
        validCheck = true;
        // Elektronska
        System.out.println("Vpisite elektronsko pošto");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavnaElektronskaPosta(inputString)) {
                noviKontakt.setElektronskaPosta(inputString);
                validCheck = false;
            }
        }
        validCheck = true;
        System.out.println("Vpisite telefonsko številko");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavniTelefon(inputString)) {
                noviKontakt.setTelefon(inputString);
                validCheck = false;
            }
        }
        validCheck = true;
        System.out.println("Vpisite mobilno številko");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavniMobilniTelefon(inputString)) {
                noviKontakt.setMobilniTelefon(inputString);
                validCheck = false;
            }
        }
        validCheck = true;
        System.out.println("Vpisite opombo");
        while (validCheck) {
            inputString = in.nextLine();
            if (inputString == "Exit") {
                return;
            }
            if (Kontakt.veljavnaOpomba(inputString)) {
                noviKontakt.setOpomba(inputString);
                validCheck = false;
            }
        }
        int indexClone = vrniIndexKontakta(noviKontakt.getId());
        if (indexClone != -1) {
            System.out.println("Kontakt z tem ID že obstaja.");
            return;
        }
        if (IsDuplicate(noviKontakt)) {
            System.out.println("Kontakt je že dodan.");
            return;
        }
        seznamKontaktov.add(noviKontakt);
    }

    /**
     * Preveri če je konktak duplikat katerega kontakta iz seznama kontaktov
     * 
     */
    private boolean IsDuplicate(Kontakt noviKontakt) {
        for (Kontakt kontakt : seznamKontaktov) {
            if (kontakt.equals(noviKontakt) || kontakt.getId() == noviKontakt.getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda popravi podatke na obstoječem kontaktu ID kontakta ni mogoče
     * spreminjati
     */
    public void urediKontakt(Scanner in) {
        System.out.println("ID kontakta:");
        String input = in.nextLine();

        if (!IsPositiveNumber(input)) {
            System.out.println("Vnos ni veljaven!");
            return;
        }
        int index = vrniIndexKontakta(Integer.parseInt(input));
        if (index == -1) {
            System.out.println("Kontakt z tem ID ne obstaja.");
            return;
        }

        izpisiMenuZaUrejanjeKontakta();

        String akcija = "";
        String inputString = "";

        while (!"0".equals(akcija)) {
            akcija = in.nextLine();
            switch (akcija) {
                case "1":
                    System.out.println("Vpišite novo ime:");
                    inputString = in.nextLine();
                    if (Kontakt.veljavnoIme(inputString)) {
                        seznamKontaktov.get(index).setIme(inputString);
                    }
                    break;
                case "2":
                    System.out.println("Vpišite novi Priimek:");
                    inputString = in.nextLine();
                    if (Kontakt.veljavniPriimek(inputString)) {
                        seznamKontaktov.get(index).setPriimek(inputString);
                    }
                    break;
                case "3":
                    System.out.println("Vpišite novi Naslov");
                    inputString = in.nextLine();
                    if (Kontakt.veljavniNaslov(inputString)) {
                        seznamKontaktov.get(index).setNaslov(inputString);
                    }
                    break;
                case "4":
                    System.out.println("Vpišite novo Elektronsko pošto");
                    inputString = in.nextLine();
                    if (Kontakt.veljavnaElektronskaPosta(inputString)) {
                        seznamKontaktov.get(index).setElektronskaPosta(inputString);
                    }
                    break;
                case "5":
                    System.out.println("Vpišite novi telefon");
                    inputString = in.nextLine();
                    if (Kontakt.veljavniTelefon(inputString)) {
                        seznamKontaktov.get(index).setTelefon(inputString);
                    }
                    // phone
                    break;
                case "6":
                    System.out.println("Vpišite novi številko mobilnega telefona");
                    inputString = in.nextLine();
                    if (Kontakt.veljavniMobilniTelefon(inputString)) {
                        seznamKontaktov.get(index).setMobilniTelefon(inputString);
                    }
                    break;
                case "7":
                    inputString = in.nextLine();
                    if (Kontakt.veljavnaOpomba(inputString)) {
                        seznamKontaktov.get(index).setOpomba(inputString);
                    }
                    break;
                default:
                    System.out.println("Napačna izbira!!!");
                    break;
            }
            izpisiMenuZaUrejanjeKontakta();
        }
        // Check for duplicates
        System.out.println("FinishEditin");
    }

    /**
     * Izpiše menu za urejanje kontakta
     */
    public static void izpisiMenuZaUrejanjeKontakta() {

        System.out.println("Kaj želite spremeniti?");
        System.out.println("-----------------------------------");
        System.out.println("0 - Končaj urejanje");
        System.out.println("1 - Ime");
        System.out.println("2 - Priimek");
        System.out.println("3 - Naslov");
        System.out.println("4 - Elektronsko pošto");
        System.out.println("5 - Telefonsko številko");
        System.out.println("6 - Mobilno številko");
        System.out.println("7 - Opombo");
        System.out.println("----------------------------------");

    }

    /**
     * Brisanje kontakta po ID-ju
     */
    public void izbrisiKontaktPoId(Scanner in) {
        String input = in.nextLine();
        if (!IsPositiveNumber(input)) {
            System.out.println("Vnos ni veljaven!");
            return;
        }
        int index = vrniIndexKontakta(Integer.parseInt(input));
        if (index == -1) {
            System.out.println("Ni kontakta z id:" + in);
            return;
        }
        seznamKontaktov.remove(index);
    }

    /**
     * Vrne index kontakta v seznamu kontaktev
     */
    public int vrniIndexKontakta(int ID) {
        int index = -1;
        for (int i = 0; i < seznamKontaktov.size(); i++) {
            if (seznamKontaktov.get(i).getId() == ID) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Izpis kontakta po ID-ju
     */
    public void izpisiKontaktZaId(Scanner in) {
        String input = in.nextLine();
        if (!IsPositiveNumber(input)) {
            System.out.println("Vnos ni veljaven!");
            return;
        }
        int index = vrniIndexKontakta(Integer.parseInt(input));
        if (index == -1) {
            System.out.println("Ni kontakta z id:" + in);
            return;
        }
        System.out.println(seznamKontaktov.get(index));
    }

    /**
     * Izpis kontakta po ID-ju
     */
    public void izpisiSteviloKontaktov() {
        System.out.println("Število kontaktov:" + seznamKontaktov.size());
    }

    /**
     * Serializiraj seznam kontoktov na disk. Ime datoteke naj bo "kontakti.ser"
     */
    public void serializirajSeznamKontaktov() {
        try {
            FileOutputStream fos = new FileOutputStream("data.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(seznamKontaktov);
            oos.close();
            fos.close();
            System.out.println("Data writen");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Pereberi serializiran seznam kontakotv iz diska
     * 
     */
    public boolean naloziSerializiranSeznamKontakotv() {
        try {
            FileInputStream fis = new FileInputStream("data.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);

            seznamKontaktov = (ArrayList<Kontakt>) ois.readObject();

            ois.close();
            fis.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
            return false;
        }
        System.out.println("Podatki iz datoteke pridobljeni.");
        return true;
    }

    /**
     * Izvozi seznam kontakov CSV datoteko. Naj uporabnik sam izbere ime izhodne
     * datoteke.
     */
    public void izvoziPodatkeVCsvDatoteko() {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get("data.csv"));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);

            for (Kontakt kontakt : seznamKontaktov) {
                csvPrinter.printRecord(String.valueOf(kontakt.getId()), kontakt.getIme(), kontakt.getPriimek(),
                        kontakt.getNaslov(), kontakt.getElektronskaPosta(), kontakt.getTelefon(),
                        kontakt.getMobilniTelefon(), kontakt.getOpomba());

            }

            csvPrinter.flush();
            csvPrinter.close();
            System.out.println("CSV file exported.");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return;
        }

    }

    /**
     * Naloži kontakte iz podatkovne baze in nadomesti trenutene kontakte
     * 
     */
    public boolean naloziPodatkeIzPodatkovneBaze(String databaseURL) {

        try (Connection conn = DriverManager.getConnection(databaseURL);
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(selectSql);) {
            seznamKontaktov.clear();
            while (rs.next()) {
                Kontakt noviKontakt = new Kontakt(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8));
                seznamKontaktov.add(noviKontakt);
            }
            System.out.println("Podatki iz baze pridobljeni.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Podatki iz baze niso bili pridobljeni.");
        return false;
    }

    /**
     * Nadomesti kontakte v bazi z trenutnimi kontakti
     * 
     */
    public void izvoziPodatkeVPodatkovnoBazo(String databaseURL) {

        Connection connection = null;
        // Connect
        try {
            connection = DriverManager.getConnection(databaseURL);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Empty database
        try (Statement stmt = connection.createStatement();) {
            stmt.executeUpdate(deleteQuery);
            System.out.println("Kontakti izbrisani iz podatkovne baze.");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Insert data
        try (PreparedStatement pstmt = connection.prepareStatement(insertQuery);) {
            for (Kontakt kontakt : seznamKontaktov) {
                pstmt.setInt(1, kontakt.getId());
                pstmt.setString(2, kontakt.getIme());
                pstmt.setString(3, kontakt.getPriimek());
                pstmt.setString(4, kontakt.getNaslov());
                pstmt.setString(5, kontakt.getElektronskaPosta());
                pstmt.setString(6, kontakt.getTelefon());
                pstmt.setString(7, kontakt.getMobilniTelefon());
                pstmt.setString(8, kontakt.getOpomba());
                pstmt.executeUpdate();
            }
            System.out.println("Kontakti shranjeni v podatkovno bazo.");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Disconnect
        try {
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Preveri če je v stringu pozitivno število
     * 
     */
    private boolean IsPositiveNumber(String s) {
        Matcher m = ValidNumberPattern.matcher(s);
        if (!m.matches()) {
            return false;
        }
        return true;
    }
}
